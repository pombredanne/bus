#!/usr/bin/python2
# -*- coding: utf-8 -*-

from math import floor
try:
    import pygame_sdl2 as pygame
    pygame.import_as_pygame()
except ImportError:
    import pygame

pygame.init()


def getasiento(x, y, col, fil, anch):
    """Retorna un índice

    Usa una variante de la formula para hasheo espacial (spatial hashing) para
    obtener el índice de determinada cuadricula en una lista sencilla.

    ¿La inversa de esta formula? (ingresa el índice, retorna las coordenadas X
    y Y) Pues, ni idea...

    """
    floorx = floor(x / anch)
    floory = floor(y / anch)
    # Si al resultado no se le suma `(COLUMNA - 1) * floory` lo que sucederá es
    # que el índice no sera el correcto. Aún no sé porque funciona, pero
    # funciona...
    r = int((floorx + floory) + ((col - 1) * floory))

    return r


def genlista(col, fil, anch):
    if fil < 3:
        # Las filas no pueden ser menores a tres porque necesitamos un espacio
        # para el chofer del bus
        fil = 3
    # esto es una comprensión de lista, es para generar la lista en una sola
    # linea de código.
    lista = [1 for i in xrange(0, columnas * filas)]

    # hacemos el primer pasillo y el pasillo de atrás
    for i in xrange(0, anch * 3, anch):
        y = getasiento(anch * (col - 2), i, col, fil, anch)
        x = getasiento(0, i, col, fil, anch)
        lista[x] = 0
        lista[y] = 0

    # hace pasillo horizontal, el que conecta los dos pasillos verticales
    for j in xrange(0, anch * (col - 1), anch):
        z = getasiento(j, anch * 2, col, fil, anch)
        lista[z] = 0

    # crea el chofer
    c = getasiento(0, anch * 3, col, fil, anch)
    lista[c] = 4

    # retorna la lista con las modificaciones que hicimos
    return lista

fontname = pygame.font.match_font("dejavusans")
font = pygame.font.Font(fontname, 11)

columnas = 14
filas = 5
ancho = 64

negro = (0, 0, 0)  # piso; 0
rojo = (255, 0, 0)  # asiento vacio; 1
amarillo = (255, 255, 0)  # asiento reservado; 2
azul = (0, 0, 255)  # asiento discapacitado; 3
gris = (155, 155, 155)  # conductor; 4

pygame.init()

# pantalla: ancho y alto
size = width, height = (columnas * ancho) + 4, (filas * ancho) + 4
screen = pygame.display.set_mode(size)

asientos = genlista(columnas, filas, ancho)

while True:
    # de aquí en adelante, es presentación de los datos.
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            # Calcula el elemento de la lista segun la posicion donde el
            # puntero ha hecho click
            x, y = pygame.mouse.get_pos()
            print "X: {} - Y: {}".format(x, y)
            r = getasiento(x, y, columnas, filas, ancho)

            try:
                if asientos[r] != 0 and asientos[r] != 4:
                    asientos[r] += 1
                    if asientos[r] > 3:
                        asientos[r] = 1
            except IndexError:
                pass

    # limpia la pantalla
    screen.fill(negro)

    for x in xrange(0, columnas * ancho, ancho):
        for y in xrange(0, filas * ancho, ancho):
            i = getasiento(x, y, columnas, filas, ancho)
            numsurf = font.render(str(int(i)), True, (255, 255, 255))

            xsurf = font.render(str(x), True, (255, 255, 255))
            xrect = pygame.Rect(
                x + 2, y + 2 + font.get_height(), ancho - 2, ancho - 2)

            ysurf = font.render(str(y), True, (255, 255, 255))
            yrect = pygame.Rect(
                x + 2, (y + 2) + (font.get_height() * 2), ancho - 2, ancho - 2)

            rect = pygame.Rect(x + 2, y + 2, ancho - 2, ancho - 2)

            try:
                if asientos[i] == 0:
                    pygame.draw.rect(screen, negro, rect)
                if asientos[i] == 1:
                    pygame.draw.rect(screen, rojo, rect)
                elif asientos[i] == 2:
                    pygame.draw.rect(screen, amarillo, rect)
                elif asientos[i] == 3:
                    pygame.draw.rect(screen, azul, rect)
                elif asientos[i] == 4:
                    pygame.draw.rect(screen, gris, rect)
            except IndexError:
                pass

            screen.blit(numsurf, rect)
            screen.blit(xsurf, xrect)
            screen.blit(ysurf, yrect)

    pygame.display.flip()
